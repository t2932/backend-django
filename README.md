# Template Backend Django API
this is a simple template for generate new APIs whitout having to setup all the project and framework (django-rest-framework) from scratch.

## Setup
To setup a new project from the template, simply follow this steps :

```bash
# create new folder to put your project
mkdir new_folder
cd new_folder

# create a virtual environment
python -m venv venv
source venv/bin/activate

# install django
pip install django

# create a new project from the template (you can change the link as you wish from differents releases)
django-admin startproject --template https://gitlab.com/t2932/backend-django/-/archive/1.2.1/backend-django-1.2.1.zip backend_new_app .

# install dependances
pip install -r requirements.txt

# setup the database and apply the migrations
python manage.py makemigrations
python manage.py migrate

# create a superuser
python manage.py createsuperuser

# you can now start the developpement server
python manage.py runserver

# you can now call the API at : http://localhost:8000

# call /api-token-auth to get your user token
curl -X POST -F 'username=user_name' -F 'password=password' http://localhost:8000/api-token-auth/
```

If the connection between the front and the back cannot be established, add this (or onother url you're using to access the frontend) to the [settings.py file](project_name/settings.py) :
```python
CORS_ALLOWED_ORIGINS = [
    'http://localhost:5173'
]
```