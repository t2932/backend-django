from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from api.models import *
from django.http import JsonResponse

import logging
LOGGER = logging.getLogger("django")

@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated,))
def simple_view(request):
    current = {"test": "yes"}
    return JsonResponse(current, safe=False)
